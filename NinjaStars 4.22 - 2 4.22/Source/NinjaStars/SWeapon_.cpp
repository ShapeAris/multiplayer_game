// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeapon_.h"

// Sets default values
ASWeapon_::ASWeapon_()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASWeapon_::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASWeapon_::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

