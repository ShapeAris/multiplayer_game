// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NinjaCharacter.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "NinjaPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class NINJASTARS_API ANinjaPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	public:
	ANinjaCharacter* GetControlledNinja() const;

	void BeginPlay() override;

};
