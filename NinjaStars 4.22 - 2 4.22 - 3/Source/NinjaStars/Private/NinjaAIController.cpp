// Fill out your copyright notice in the Description page of Project Settings.


#include "NinjaAIController.h"

void ANinjaAIController::BeginPlay()
{
    Super::BeginPlay();

    auto PlayerNinja = GetPlayerNinja();
    if(!PlayerNinja)
    {
        UE_LOG(LogTemp, Warning, TEXT("AI cannot find Player Tank"))
    } else {
        UE_LOG(LogTemp, Warning, TEXT("AI found player:  %s"), *(PlayerNinja->GetName()));
    }   
}

ANinjaCharacter* ANinjaAIController::GetControlledNinja() const
{
    return Cast<ANinjaCharacter>(GetPawn());
}

ANinjaCharacter* ANinjaAIController::GetPlayerNinja() const
{

    auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
    if(!PlayerPawn) { return nullptr;}
    return Cast<ANinjaCharacter>(PlayerPawn);
}

