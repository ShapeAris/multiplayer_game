// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NinjaCharacter.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "NinjaAIController.generated.h"

/**
 * 
 */
UCLASS()
class NINJASTARS_API ANinjaAIController : public AAIController
{
	GENERATED_BODY()

	void BeginPlay() override;

	ANinjaCharacter* GetControlledNinja() const;

	ANinjaCharacter* GetPlayerNinja() const;

	ANinjaCharacter* GetLocalPlayer() const;
};
