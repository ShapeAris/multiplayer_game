// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeaponShuriken.h"

// Sets default values
ASWeaponShuriken::ASWeaponShuriken()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASWeaponShuriken::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASWeaponShuriken::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

