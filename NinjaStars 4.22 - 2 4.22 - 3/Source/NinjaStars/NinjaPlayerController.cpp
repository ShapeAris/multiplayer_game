// Fill out your copyright notice in the Description page of Project Settings.


#include "NinjaPlayerController.h"

void ANinjaPlayerController::BeginPlay()
{
    Super::BeginPlay();

    auto ControlledNinja = GetControlledNinja();
    if(!ControlledNinja)
    {
        UE_LOG(LogTemp, Warning, TEXT("PC Not Controlled"))
    } else {
        UE_LOG(LogTemp, Warning, TEXT("PC Controlling:  %s"), *(ControlledNinja->GetName()));
    }   
}

ANinjaCharacter* ANinjaPlayerController::GetControlledNinja() const
{
    return Cast<ANinjaCharacter>(GetPawn());
}

