// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NINJASTARS_SWeaponed_generated_h
#error "SWeaponed.generated.h already included, missing '#pragma once' in SWeaponed.h"
#endif
#define NINJASTARS_SWeaponed_generated_h

#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_RPC_WRAPPERS
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASWeaponed(); \
	friend struct Z_Construct_UClass_ASWeaponed_Statics; \
public: \
	DECLARE_CLASS(ASWeaponed, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeaponed)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASWeaponed(); \
	friend struct Z_Construct_UClass_ASWeaponed_Statics; \
public: \
	DECLARE_CLASS(ASWeaponed, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeaponed)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASWeaponed(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASWeaponed) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeaponed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeaponed); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeaponed(ASWeaponed&&); \
	NO_API ASWeaponed(const ASWeaponed&); \
public:


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeaponed(ASWeaponed&&); \
	NO_API ASWeaponed(const ASWeaponed&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeaponed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeaponed); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASWeaponed)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_PRIVATE_PROPERTY_OFFSET
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_9_PROLOG
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_RPC_WRAPPERS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_INCLASS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_INCLASS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NINJASTARS_API UClass* StaticClass<class ASWeaponed>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeaponed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
