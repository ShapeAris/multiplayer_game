// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NINJASTARS_SWeapon_generated_h
#error "SWeapon.generated.h already included, missing '#pragma once' in SWeapon.h"
#endif
#define NINJASTARS_SWeapon_generated_h

#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	}


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	}


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASWeapon(); \
	friend struct Z_Construct_UClass_ASWeapon_Statics; \
public: \
	DECLARE_CLASS(ASWeapon, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeapon)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASWeapon(); \
	friend struct Z_Construct_UClass_ASWeapon_Statics; \
public: \
	DECLARE_CLASS(ASWeapon, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeapon)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeapon(ASWeapon&&); \
	NO_API ASWeapon(const ASWeapon&); \
public:


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeapon(ASWeapon&&); \
	NO_API ASWeapon(const ASWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASWeapon)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComp() { return STRUCT_OFFSET(ASWeapon, MeshComp); } \
	FORCEINLINE static uint32 __PPO__DamageType() { return STRUCT_OFFSET(ASWeapon, DamageType); } \
	FORCEINLINE static uint32 __PPO__TraceEnd() { return STRUCT_OFFSET(ASWeapon, TraceEnd); }


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_11_PROLOG
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_RPC_WRAPPERS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_INCLASS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_INCLASS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NINJASTARS_API UClass* StaticClass<class ASWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
