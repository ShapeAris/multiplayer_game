// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NinjaStars/SWeaponShuriken.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSWeaponShuriken() {}
// Cross Module References
	NINJASTARS_API UClass* Z_Construct_UClass_ASWeaponShuriken_NoRegister();
	NINJASTARS_API UClass* Z_Construct_UClass_ASWeaponShuriken();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_NinjaStars();
// End Cross Module References
	void ASWeaponShuriken::StaticRegisterNativesASWeaponShuriken()
	{
	}
	UClass* Z_Construct_UClass_ASWeaponShuriken_NoRegister()
	{
		return ASWeaponShuriken::StaticClass();
	}
	struct Z_Construct_UClass_ASWeaponShuriken_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASWeaponShuriken_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NinjaStars,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASWeaponShuriken_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SWeaponShuriken.h" },
		{ "ModuleRelativePath", "SWeaponShuriken.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASWeaponShuriken_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASWeaponShuriken>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASWeaponShuriken_Statics::ClassParams = {
		&ASWeaponShuriken::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ASWeaponShuriken_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASWeaponShuriken_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASWeaponShuriken()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASWeaponShuriken_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASWeaponShuriken, 2664079708);
	template<> NINJASTARS_API UClass* StaticClass<ASWeaponShuriken>()
	{
		return ASWeaponShuriken::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASWeaponShuriken(Z_Construct_UClass_ASWeaponShuriken, &ASWeaponShuriken::StaticClass, TEXT("/Script/NinjaStars"), TEXT("ASWeaponShuriken"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASWeaponShuriken);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
