// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NINJASTARS_SWeapon__generated_h
#error "SWeapon_.generated.h already included, missing '#pragma once' in SWeapon_.h"
#endif
#define NINJASTARS_SWeapon__generated_h

#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_RPC_WRAPPERS
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASWeapon_(); \
	friend struct Z_Construct_UClass_ASWeapon__Statics; \
public: \
	DECLARE_CLASS(ASWeapon_, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeapon_)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_INCLASS \
private: \
	static void StaticRegisterNativesASWeapon_(); \
	friend struct Z_Construct_UClass_ASWeapon__Statics; \
public: \
	DECLARE_CLASS(ASWeapon_, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NinjaStars"), NO_API) \
	DECLARE_SERIALIZER(ASWeapon_)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASWeapon_(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASWeapon_) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeapon_); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeapon_); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeapon_(ASWeapon_&&); \
	NO_API ASWeapon_(const ASWeapon_&); \
public:


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeapon_(ASWeapon_&&); \
	NO_API ASWeapon_(const ASWeapon_&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeapon_); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeapon_); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASWeapon_)


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_PRIVATE_PROPERTY_OFFSET
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_9_PROLOG
#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_RPC_WRAPPERS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_INCLASS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_PRIVATE_PROPERTY_OFFSET \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_INCLASS_NO_PURE_DECLS \
	NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NINJASTARS_API UClass* StaticClass<class ASWeapon_>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NinjaStars_4_22___2_4_22___3_Source_NinjaStars_SWeapon__h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
