// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NinjaStars/NinjsPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNinjsPawn() {}
// Cross Module References
	NINJASTARS_API UClass* Z_Construct_UClass_ANinjsPawn_NoRegister();
	NINJASTARS_API UClass* Z_Construct_UClass_ANinjsPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_NinjaStars();
// End Cross Module References
	void ANinjsPawn::StaticRegisterNativesANinjsPawn()
	{
	}
	UClass* Z_Construct_UClass_ANinjsPawn_NoRegister()
	{
		return ANinjsPawn::StaticClass();
	}
	struct Z_Construct_UClass_ANinjsPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANinjsPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_NinjaStars,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANinjsPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "NinjsPawn.h" },
		{ "ModuleRelativePath", "NinjsPawn.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANinjsPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANinjsPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANinjsPawn_Statics::ClassParams = {
		&ANinjsPawn::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ANinjsPawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANinjsPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANinjsPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANinjsPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANinjsPawn, 4030597160);
	template<> NINJASTARS_API UClass* StaticClass<ANinjsPawn>()
	{
		return ANinjsPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANinjsPawn(Z_Construct_UClass_ANinjsPawn, &ANinjsPawn::StaticClass, TEXT("/Script/NinjaStars"), TEXT("ANinjsPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANinjsPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
